var express = require('express'),
  _ =  require('underscore'),
  debugPlatform = require('debug')('platform'),
  debugSupport = require('debug')('support'),
  plivo = require('plivo'),
  http = require('http'),
  bodyParser = require('body-parser'),
  errorHandler = require('errorhandler'),
  morgan = require('morgan'), //express logger
  app,
  api;

app = express();

app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

//development error logging
app.use(morgan('dev'));
app.use(errorHandler());
app.server = http.createServer(app);

api = plivo.RestAPI({
  'authId': 'MANWY0NJUZZDVHMJQYOT',
  'authToken': 'NWQzNGE3ODk2YjhhZTAyYjEwN2M4NTkzNGI3ZTg2',
});

var currentCalls = {};

//platform
app.all('/hello', function(req, res) {
  //call another sip support
  //call my phone number
  if(req.body.Direction.toLowerCase() === 'inbound') {
    var numberPattern = /\d+/g;
    var from = req.body.From.match(numberPattern)[0];
    currentCalls[from] = {};
    var params1 = {
      from: from,
      to: '33630059901',
      answer_url:'http://178.62.186.203:3000/answer_url',
      ring_url: 'http://178.62.186.203:3000/ring_url'
    };

    var params2 = {
      from: from,
      to: 'sip:support141028101454@phone.plivo.com',
      answer_url:'http://178.62.186.203:3000/answer_url',
      ring_url: 'http://178.62.186.203:3000/ring_url'
    };

    api.make_call(params1, function(status, response) {
      if (status >= 200 && status < 300) {
        debugPlatform('Successfully made call request.');
        currentCalls[from][response.request_uuid] = '';
        debugPlatform('first call request uuid:', response.request_uuid);
      } else {
        debugPlatform('Oops! Something went wrong.');
        debugPlatform('Status:', status);
        debugPlatform('Make Call 1 Response:', response);
      }
    });

    api.make_call(params2, function(status, response) {
      if (status >= 200 && status < 300) {
        debugPlatform('Successfully made call request.');
        currentCalls[from][response.request_uuid] = '';
        debugPlatform('second call request uuid:', response.request_uuid);
      } else {
        debugPlatform('Oops! Something went wrong.');
        debugPlatform('Status:', status);
        debugPlatform('Make call 2 Response:', response);
      }
    }); 
  }
 
});

app.all('/answer_url', function(req, res) {
  if(req.body.Event.toLowerCase() === 'startapp') {
    debugPlatform('startapp', req.body);
    //hang up the other call
    var numberPattern = /\d+/g,
      from = req.body.From.match(numberPattern)[0],
      currentCallRequestUUID = req.body.RequestUUID;
    _.map(currentCalls[from], function(callUUID, requestUUID) {
      if(currentCallRequestUUID !== requestUUID) {
        if(callUUID !== '') {
          debugPlatform('answer url callUUID', callUUID);
          api.hangup_call({call_uuid: callUUID}, function(status, response) {
            if (status >= 200 && status < 300) {
              debugPlatform('Successfully hang a call request.');
            } else {
              debugPlatform('Oops! Something went wrong. while hangup_call');
              debugPlatform('Status:', status);
              debugPlatform('Hang Up Response:', response);
            }
          });
          delete currentCalls[from][requestUUID];
        }
      }
    });
  }
});

app.all('/ring_url', function(req, res) {
  if(req.body.CallStatus.toLowerCase() === 'ringing') {
    var numberPattern = /\d+/g,
      from = req.body.From.match(numberPattern)[0];
    if(typeof currentCalls[from] !== 'undefined') {
      currentCalls[from][req.body.RequestUUID] = req.body.CallUUID;
    }
  }
});

app.all('/bye', function(req, res) {
  debugPlatform('bye platform');
});

app.all('/sms', function(req, res) {
  debugPlatform('sms platform');
});

app.all('/error', function(req, res) {
  debugPlatform('error platform', req.body);
});


//support
app.all('/hello1', function(req, res) {
  debugSupport('hello1');
});

app.all('/bye1', function(req, res) {
  debugSupport('bye1');
});

app.all('/sms1', function(req, res) {
  debugSupport('sms1');
});

app.all('/error1', function(req, res) {
  debugSupport('error1', req.body);
});

app.set('port', 3000);

app.server.listen(app.get('port'), function () {
  console.log("Express server listening on port " + app.get('port'));
});
